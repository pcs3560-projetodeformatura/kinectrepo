/*
 * This file is part of the OpenKinect Project. http://www.openkinect.org
 *
 * Copyright (c) 2010 individual OpenKinect contributors. See the CONTRIB file
 * for details.
 *
 * This code is licensed to you under the terms of the Apache License, version
 * 2.0, or, at your option, the terms of the GNU General Public License,
 * version 2.0. See the APACHE20 and GPL2 files for the text of the licenses,
 * or the following URLs:
 * http://www.apache.org/licenses/LICENSE-2.0
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you redistribute this file in source form, modified or unmodified, you
 * may:
 *   1) Leave this header intact and distribute it under the same terms,
 *      accompanying it with the APACHE20 and GPL20 files, or
 *   2) Delete the Apache 2.0 clause and accompany it with the GPL2 file, or
 *   3) Delete the GPL v2 clause and accompany it with the APACHE20 file
 * In all cases you must keep the copyright notice intact and include a copy
 * of the CONTRIB file.
 *
 * Binary distributions must follow the binary distribution requirements of
 * either License.
 */
#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/plane_clipper3D.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/extract_indices.h>

#include <pcl/common/transforms.h>


#include <cstdlib>
#include <iostream>
#include <vector>
#include <pthread.h>
#include <libfreenect.hpp>
#include <math.h>
#include "pcl/common/common_headers.h"
#include "pcl/io/pcd_io.h"
#include "pcl/visualization/pcl_visualizer.h"
#include "pcl/console/parse.h"
#include "pcl/point_types.h"
#include "boost/lexical_cast.hpp"
#include <string>
#include <algorithm>

#if defined(__APPLE__)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#define PI 3.14159265

class Mutex
{
public:
    Mutex()
    {
        pthread_mutex_init(&m_mutex, NULL);
    }

    void lock()
    {
        pthread_mutex_lock(&m_mutex);
    }

    void unlock()
    {
        pthread_mutex_unlock(&m_mutex);
    }

    class ScopedLock
    {
    public:
        ScopedLock(Mutex &mutex) : _mutex(mutex)
        {
            _mutex.lock();
        }

        ~ScopedLock()
        {
            _mutex.unlock();
        }

    private:
        Mutex &_mutex;
    };

private:
    pthread_mutex_t m_mutex;
};


class MyFreenectDevice : public Freenect::FreenectDevice
{
public:
    MyFreenectDevice(freenect_context *_ctx, int _index)
        : Freenect::FreenectDevice(_ctx, _index),
          m_buffer_video(freenect_find_video_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_VIDEO_RGB).bytes),
          m_buffer_depth(freenect_find_depth_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_DEPTH_REGISTERED).bytes / 2),
          m_new_rgb_frame(false), m_new_depth_frame(false)
    {
        setDepthFormat(FREENECT_DEPTH_REGISTERED);
    }

    // Do not call directly, even in child
    void VideoCallback(void *_rgb, uint32_t timestamp)
    {
        Mutex::ScopedLock lock(m_rgb_mutex);
        uint8_t* rgb = static_cast<uint8_t*>(_rgb);
        copy(rgb, rgb+getVideoBufferSize(), m_buffer_video.begin());
        m_new_rgb_frame = true;
    }

    // Do not call directly, even in child
    void DepthCallback(void *_depth, uint32_t timestamp)
    {
        Mutex::ScopedLock lock(m_depth_mutex);
        uint16_t* depth = static_cast<uint16_t*>(_depth);
        copy(depth, depth+getDepthBufferSize()/2, m_buffer_depth.begin());
        m_new_depth_frame = true;
    }

    bool getRGB(std::vector<uint8_t> &buffer)
    {
        Mutex::ScopedLock lock(m_rgb_mutex);

        if (!m_new_rgb_frame)
            return false;

        buffer.swap(m_buffer_video);
        m_new_rgb_frame = false;

        return true;
    }

    bool getDepth(std::vector<uint16_t> &buffer)
    {
        Mutex::ScopedLock lock(m_depth_mutex);

        if (!m_new_depth_frame)
            return false;

        buffer.swap(m_buffer_depth);
        m_new_depth_frame = false;

        return true;
    }

private:
    Mutex m_rgb_mutex;
    Mutex m_depth_mutex;
    std::vector<uint8_t> m_buffer_video;
    std::vector<uint16_t> m_buffer_depth;
    bool m_new_rgb_frame;
    bool m_new_depth_frame;
};


// template <typename T>
// std::string ToString(T val)
// {
//     std::stringstream stream;
//     stream << val;
//     return stream.str();
// }


Freenect::Freenect freenect;
MyFreenectDevice* device;
Freenect::FreenectTiltState *tilt_state_pointer;

int main(int argc, char **argv)
{
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_filtered (new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_projected(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_projected_xy(new pcl::PointCloud<pcl::PointXYZRGB>);
    

    cloud->is_dense=false;
    cloud_filtered->is_dense = false;
    cloud_projected->is_dense = false;
    // Create and setup the viewer
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer2(new pcl::visualization::PCLVisualizer("3D Viewer2"));
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer3(new pcl::visualization::PCLVisualizer("3D Viewer3"));
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer4(new pcl::visualization::PCLVisualizer("3D Viewer4"));
    

    std::vector<pcl::visualization::Camera> cam;
    // viewer->registerKeyboardCallback(keyboardEventOccurred, (void *)&viewer);
    viewer->setBackgroundColor(0, 0, 0);
    viewer->addPointCloud<pcl::PointXYZRGB>(cloud, "Kinect Cloud");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,
                                             1, "Kinect Cloud");
    viewer->initCameraParameters();
    viewer->addCoordinateSystem(1.0);

    viewer2->setBackgroundColor(255, 255, 255);
    viewer2->addPointCloud<pcl::PointXYZRGB>(cloud_filtered, "Kinect Cloud");
    viewer2->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,
                                              1, "Kinect Cloud");
    viewer2->addCoordinateSystem(1.0);
    viewer2->initCameraParameters();

    viewer3->setBackgroundColor(0, 0, 0);
    viewer3->addPointCloud<pcl::PointXYZRGB>(cloud_projected, "Kinect Cloud");
    viewer3->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,
                                              1, "Kinect Cloud");
    viewer3->addCoordinateSystem(1.0);
    viewer3->initCameraParameters();

    viewer4->setBackgroundColor(0, 0, 0);
    viewer4->addPointCloud<pcl::PointXYZRGB>(cloud_projected_xy, "Kinect Cloud");
    viewer4->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,
                                              1, "Kinect Cloud");
    viewer4->addCoordinateSystem(1.0);
    viewer4->initCameraParameters();

    pcl::PointIndices::Ptr inliers_filtered(new pcl::PointIndices());
    pcl::ExtractIndices<pcl::PointXYZRGB> extract;
    

    // desenha eixos
    pcl::PointXYZ p1, p2;
    p1.x = 0;
    p1.y = 0;
    p1.z = 0;
    p2.x = 1000;
    p2.y = 0;
    p2.z = 0;
    viewer->addArrow<pcl::PointXYZ>(p2, p1, 1.0, 0.0, 0.0, true, "x axis");
    viewer2->addArrow<pcl::PointXYZ>(p2, p1, 1.0, 0.0, 0.0, true, "x axis");
    viewer3->addArrow<pcl::PointXYZ>(p2, p1, 1.0, 0.0, 0.0, true, "x axis");
    viewer4->addArrow<pcl::PointXYZ>(p2, p1, 1.0, 0.0, 0.0, true, "x axis");
    

    p2.x = 0;
    p2.y = 1000;
    p2.z = 0;
    viewer->addArrow<pcl::PointXYZ>(p2, p1, 0.0, 1.0, 0.0, true, "y axis");
    viewer2->addArrow<pcl::PointXYZ>(p2, p1, 0.0, 1.0, 0.0, true, "y axis");
    viewer3->addArrow<pcl::PointXYZ>(p2, p1, 0.0, 1.0, 0.0, true, "y axis");
    viewer4->addArrow<pcl::PointXYZ>(p2, p1, 0.0, 1.0, 0.0, true, "y axis");
    
    

    p2.x = 0;
    p2.y = 0;
    p2.z = 1000;
    viewer->addArrow<pcl::PointXYZ>(p2, p1, 0.0, 0.0, 1.0, true, "z axis");
    viewer2->addArrow<pcl::PointXYZ>(p2, p1, 0.0, 0.0, 1.0, true, "z axis");
    viewer3->addArrow<pcl::PointXYZ>(p2, p1, 0.0, 0.0, 1.0, true, "z axis");
    viewer4->addArrow<pcl::PointXYZ>(p2, p1, 0.0, 0.0, 1.0, true, "z axis");
    
    // desenha matriz
    int cell_coord_z=-400;
    int cell_side = 1000.0;
    p1.x = -5000; p1.y = 0;
    p2.x =  5000; p2.y = 0;
    for (int i = 0; i<4; i++) {
        p1.z = p2.z = cell_coord_z+(i*(-cell_side));
        viewer4->addArrow<pcl::PointXYZ>(p2, p1, 1.0, 1.0, 1.0, false, std::to_string(i));
    }
    int cell_coord_x=-1500;
    p1.z = 0;     
    p2.z = -5000;
    for (int i = 0; i<4; i++) {
        p1.x = p2.x = cell_coord_x+(i*(cell_side));
        viewer4->addArrow<pcl::PointXYZ>(p2, p1, 1.0, 1.0, 1.0, false, std::to_string(i+4));
    }

    // p1.x = 1000;
    // p1.y = 0;
    // p1.z = -900;
    // viewer4->addText3D("OIASDAD", p1, 150.0, 1.0, 1.0, 1.0, "aaa");

    viewer->addCoordinateSystem(1.0);
    viewer->setCameraPosition(0.0, 0.0, -0.2, 0.0, -1.0, 0.0);
    viewer2->setCameraPosition(0.0, 0.0, -0.2, 0.0, -1.0, 0.0);
    viewer3->setCameraPosition(-350.f, -500.f, 5000.f, 0.0, 0.0, 1.0);

    viewer4->setCameraPosition(0.0, 4700.0, -2000.0, 0.0, 0.0, -1.0);


//  - pos: (-542.553, -4178.09, 4170.92)
//  - view: (-0.032026, 0.129116, 0.991112)
//  - focal: (-301.915, 134.448, 3616.88)

    device = &freenect.createDevice<MyFreenectDevice>(0);
    device->startVideo();
    device->startDepth();
    // tilt_state = device->getState();
    // tilt_state_pointer = &tilt_state;

    Freenect::FreenectTiltState tilt_state = device->getState();

    tilt_state_pointer = &tilt_state;

    static std::vector<uint16_t> mdepth(640 * 480);
    static std::vector<uint8_t> mrgb(640 * 480 * 4);

    cloud->width = 640;
    cloud->height = 480;
    cloud->points.resize(cloud->width * cloud->height);

    cloud_filtered->width = 640;
    cloud_filtered->height = 480;
    cloud_filtered->points.resize(cloud->width * cloud->height);


    cloud_projected_xy->width = 640;
    cloud_projected_xy->height = 480;
    cloud_projected_xy->points.resize(cloud->width * cloud->height);

    pcl::ProjectInliers<pcl::PointXYZRGB> proj;
    proj.setModelType(pcl::SACMODEL_PLANE);



    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);

    pcl::PointIndices::Ptr plane_clipper_inliers (new pcl::PointIndices);
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZRGB> seg;
    // Optional
    seg.setOptimizeCoefficients(true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PERPENDICULAR_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    // seg.setDistanceThreshold (0.01);
    seg.setDistanceThreshold(25);


    seg.setEpsAngle(  20.0f * (PI/180.0f) );
    // seg.setMaxIterations(50);
    seg.setInputCloud (cloud);


    int cells[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    while (!viewer->wasStopped())
    {   
 

        // cloud->clear();
        // cloud->width = 640;
        // cloud->height = 480;
        // cloud->is_dense = false;
        // cloud->points.resize(cloud->width * cloud->height);

        device->updateState();
        device->getDepth(mdepth);
        device->getRGB(mrgb);

        double x, y, z, gravity, yz_inclination, xy_inclination, sin_xy, cos_xy, sin_yz, cos_yz;

        // Freenect::FreenectTiltState tilt_state = device->getState();
        tilt_state_pointer->getAccelerometers(&x, &y, &z);
        gravity = sqrt(x*x + y*y + z*z);
        yz_inclination = asin(z/gravity) * 180/ PI;

        // xy_inclination = asin(x/gravity) * 180/ PI;
        xy_inclination = asin(x/gravity);
        
        sin_xy = x/gravity;
        cos_xy = y/gravity;

        sin_yz = z/gravity;
        cos_yz = y/gravity;


        size_t i = 0;
        size_t cinput = 0;
        for (size_t v = 0; v < 480; v++)
        {
            for (size_t u = 0; u < 640; u++, i+=1)
            {
                float f = 595.f;
                float x = (i%640 - (640-1)/2.f) * mdepth[i] / f; // X = (x - cx) * d / fx
                float y = (i/640 - (480-1)/2.f) * mdepth[i] / f; // Y = (y - cy) * d / fy
                float z = mdepth[i];                             // Z = d
                
                // float new_x = x*cos_xy + y*sin_xy;
                // float new_y = x*sin_xy + y*cos_xy;
                // y = y*cos_yz - z*sin_yz;
                // z = - y*sin_yz + z*cos_yz;

                // cloud->points[i].x = new_x;
                // cloud->points[i].y = new_y;
                cloud->points[i].x = x;
                cloud->points[i].y = y;
                cloud->points[i].z = z;
                cloud->points[i].r = mrgb[i * 3];
                cloud->points[i].g = mrgb[(i * 3) + 1];
                cloud->points[i].b = mrgb[(i * 3) + 2];
            }
        }

        // seta vetor normal de plano procurado pelo ransac
        Eigen::Vector3f axis = Eigen::Vector3f(-1.f * sin_xy, -1.f * cos_xy * cos_yz, 1.f * sin_yz); //y axis
        // Eigen::Vector3f axis = Eigen::Vector3f(0.f, -1.f * cos_yz, 1.f * sin_yz); //y axis
        
        seg.setAxis(axis);
        seg.segment (*inliers, *coefficients);

        
        // desenha plano para debugar
        pcl::ModelCoefficients plane_coeff;
        plane_coeff.values.resize(4); // We need 4 values
        plane_coeff.values[0] = axis[0] * 1000;
        plane_coeff.values[1] = axis[1] * 1000;
        plane_coeff.values[2] = axis[2] * 1000;
        plane_coeff.values[3] = 1000.f;
        viewer->addPlane(plane_coeff, "plane");

        // vao guardar parametros de plano encontrado pelo ransac
        float plane_normal_x = 0.f;
        float plane_normal_y = 0.f;
        float plane_normal_z = 0.f;
        float plane_distance = 0.f;


        // se achou 
        if (inliers->indices.size () != 0) {  //&&  fabs (coefficients->values[3]) < 1250.f && fabs (coefficients->values[3]) > 750.f){

            plane_normal_x = coefficients->values[0];
            plane_normal_y = coefficients->values[1];
            plane_normal_z = coefficients->values[2];
            plane_distance = coefficients->values[3];

            plane_normal_y = plane_normal_x*sin_xy + plane_normal_y*cos_xy;
            plane_normal_y = plane_normal_y*cos_yz - plane_normal_z*sin_yz;

            // std::cout <<  "plane_normal_y: " << plane_normal_y << std::endl;
            // std::cout <<  "plane_distance: " << plane_distance << std::endl;

            // pinta plano encontrado de vermelho
            if (fabs(plane_normal_y)>0.85 && plane_distance > 300) {
                for (size_t i = 0; i < inliers->indices.size (); ++i) {
                    cloud->points[inliers->indices[i]].r = 255.0;
                    cloud->points[inliers->indices[i]].g = 0.0;
                    cloud->points[inliers->indices[i]].b = 0.0;
                }
            }   
        }

        if (plane_normal_x != 0.f && plane_normal_y!=0.f && plane_normal_z != 0.f && plane_distance!=0.f) {

            // reseta point cloud sem o chao
            cloud_filtered->clear();
            cloud_filtered->width = 640;
            cloud_filtered->height = 480;
            cloud_filtered->is_dense = false;
            cloud_filtered->points.resize(cloud->width * cloud->height);

            // filtra point cloud com plane clipper
            Eigen::Vector4f plane_params = Eigen::Vector4f(plane_normal_x, plane_normal_y, plane_normal_z, plane_distance-30.f);
            pcl::PlaneClipper3D<pcl::PointXYZRGB> planeClipper(plane_params);
            std::vector<int> clipped; 
            planeClipper.clipPointCloud3D(*cloud, clipped); 
            extract.setInputCloud(cloud);
            extract.setIndices(boost::make_shared<std::vector<int> >(clipped));
            extract.setNegative(false);
            extract.filter(*cloud_filtered);


            // reseta point cloud projetada
            cloud_projected->clear();
            cloud_projected->width = 640;
            cloud_projected->height = 480;
            cloud_projected->is_dense = false;
            cloud_projected->points.resize(cloud->width * cloud->height);

            // projeta point cloud
            pcl::ModelCoefficients::Ptr plane_proj_coeff(new pcl::ModelCoefficients());
            plane_proj_coeff->values.resize(4);
            plane_proj_coeff->values[0] = coefficients->values[0];
            plane_proj_coeff->values[1] = coefficients->values[1];
            plane_proj_coeff->values[2] = coefficients->values[2];
            plane_proj_coeff->values[3] = coefficients->values[3];
            proj.setInputCloud(cloud_filtered);
            proj.setModelCoefficients(plane_proj_coeff);
            proj.filter (*cloud_projected);

                        // reseta point cloud projetada
            cloud_projected_xy->clear();
            cloud_projected_xy->width = 640;
            cloud_projected_xy->height = 480;
            cloud_projected_xy->is_dense = false;
            cloud_projected_xy->points.resize(cloud->width * cloud->height);

        
            Eigen::Vector3f plane_normal = Eigen::Vector3f(plane_normal_x, plane_normal_y, plane_normal_z);
            // cloud_plane_normal_vector[0] = cloud_normal->points[0].normal_x; 
            // cloud_plane_normal_vector[1] = cloud_normal->points[0].normal_y; 
            // cloud_plane_normal_vector[2] = cloud_normal->points[0].normal_z; 
            // std::cout << "Cluster_normal_vector :" << plane_normal << std::endl; 

            Eigen::Vector3f y_vector = Eigen::Vector3f(0.f, 1.f, 0.f);

            Eigen::Vector3f z_vector = Eigen::Vector3f(0.f, 0.f, 1.f);

            // xy_plane_normal_vector[0] = 0; 
            // xy_plane_normal_vector[1] = 0; 
            // xy_plane_normal_vector[2] = 1; 
            // std::cout << "Model_normal_vector :" << xy_plane_normal_vector << std::endl; 

            Eigen::Vector3f rotation_vector = plane_normal.cross (y_vector); 

            Eigen::Vector3f transformVector; 
            float length=sqrt(rotation_vector[0]*rotation_vector[0]+rotation_vector[1]*rotation_vector[1]+rotation_vector[2]*rotation_vector[2]); 
            transformVector[0] = rotation_vector[0] / length; 
            transformVector[1] = rotation_vector[1] / length; 
            transformVector[2] = rotation_vector[2] / length; 

            Eigen::Affine3f transformRotationOfModel = Eigen::Affine3f::Identity(); 

            float theta = acos(plane_normal[0]*y_vector[0]+plane_normal[1]*y_vector[1]+plane_normal[2]*y_vector[2]); 

            float theta2 = acos(plane_normal[0]*z_vector[0] + plane_normal[2]*z_vector[2]); 
            std::cout<< "angle : " << xy_inclination * 180/PI << std::endl; 

            transformRotationOfModel.rotate (Eigen::AngleAxisf (theta, transformVector)); 

            transformRotationOfModel.rotate (Eigen::AngleAxisf (4.5*xy_inclination, y_vector)); 
 
            pcl::transformPointCloud (*cloud_projected, *cloud_projected_xy, transformRotationOfModel); 

            // cells = {0, 0, 0, 0, 0, 0, 0, 0, 0};
            std::fill(cells, cells+9, 0);
            // cout << "celula 6 ANTES:" << cells[6] << endl;
            // cout << "celula 7 ANTES:" << cells[7] << endl;
            for (size_t i = 0; i < cloud_projected->size(); ++i) {
                cloud_projected_xy->points[i].y = 0.0;
                cloud_projected_xy->points[i].r = 0.f;
                cloud_projected_xy->points[i].g = 255.f;
                cloud_projected_xy->points[i].b = 0.f;
                cloud_projected->points[i].r = 0.f;
                cloud_projected->points[i].g = 255.f;
                cloud_projected->points[i].b = 0.f;


                float z = cloud_projected_xy->points[i].z;
                float x = cloud_projected_xy->points[i].x;


                float min_z = -3400.0;
                float min_x = -1500.0;
                float cell_side = 1000.0;
                int cell_index = 0;


                // for (float min_z = -3400; )

                // for (int cell_index=0; cell_index<9; cell_index++) {

                //     if (z <= ((cell_index+1)*cell_side) && z > min_z+(cell_index*cell_side) && // -2400.0  -3400.0 && // 0
                //         x >= -1500.0 && x < -500.0) {  // -1500  -500
                //         cells[cell_index]++; 
                //     }

                // }
                // conta pontos em cada celula da matriz
                if (z <= -2400.0 && z > -3400.0 && // 0
                    x >= -1500.0 && x < -500.0) { 
                    cells[0]++; 
                }

                if (z <= -2400.0 && z > -3400.0 && // 1
                    x >= -500.0 && x < 500.0) { 
                    cells[1]++; 
                }

                if (z <= -2400.0 && z > -3400.0 && // 2
                    x >= 500.0 && x < 1500.0) { 
                    cells[2]++; 
                }

                if (z <= -1400.0 && z > -2400.0 && // 3
                    x >= -1500.0 && x < -500.0) { 
                    cells[3]++; 
                }

                if (z <= -1400.0 && z > -2400.0 && // 4
                    x >= -500.0 && x < 500.0) { 
                    cells[4]++; 
                }

                if (z <= -1400.0 && z > -2400.0 && // 5
                    x >= 500.0 && x < 1500.0) { 
                    cells[5]++; 
                }

                if (z <= -400.0 && z > -1400.0 && // 6
                    x >= -1500.0 && x < -500.0) { 
                    cells[6]++; 
                }
                if (z <= -400.0 && z > -1400.0 && // 7
                    x >= -500.0 && x < 500.0) {
                    cells[7]++;
                }
                if (z <= -400.0 && z > -1400.0 && // 8
                    x >= 500.0 && x < 1500.0) {
                    cells[8]++;
                }

                          
            }
            
            float correcao=200;

            p1.x = -1000-correcao; p1.y = 0; p1.z = -2900;
            viewer4->addText3D(std::to_string(cells[0]), p1, 150.0, 1.0, 0.0, 0.0, "celula 0");

            p1.x = 0-correcao; p1.y = 0; p1.z = -2900;
            viewer4->addText3D(std::to_string(cells[1]), p1, 150.0, 1.0, 0.0, 0.0, "celula 1");

            p1.x = 1000-correcao; p1.y = 0; p1.z = -2900;
            viewer4->addText3D(std::to_string(cells[2]), p1, 150.0, 1.0, 0.0, 0.0, "celula 2");
            
            p1.x = -1000-correcao; p1.y = 0; p1.z = -1900;
            viewer4->addText3D(std::to_string(cells[3]), p1, 150.0, 1.0, 0.0, 0.0, "celula 3");

            p1.x = 0-correcao; p1.y = 0; p1.z = -1900;
            viewer4->addText3D(std::to_string(cells[4]), p1, 150.0, 1.0, 0.0, 0.0, "celula 4");

            p1.x = 1000-correcao; p1.y = 0; p1.z = -1900;
            viewer4->addText3D(std::to_string(cells[5]), p1, 150.0, 1.0, 0.0, 0.0, "celula 5");

            p1.x = -1000-correcao; p1.y = 0; p1.z = -900;
            viewer4->addText3D(std::to_string(cells[6]), p1, 150.0, 1.0, 0.0, 0.0, "celula 6");

            p1.x = 0-correcao; p1.y = 0; p1.z = -900;
            viewer4->addText3D(std::to_string(cells[7]), p1, 150.0, 1.0, 0.0, 0.0, "celula 7");

            p1.x = 1000-correcao; p1.y = 0; p1.z = -900;
            viewer4->addText3D(std::to_string(cells[8]), p1, 150.0, 1.0, 0.0, 0.0, "celula 8");

            
            // cout << "celula 6:" << cells[6] << endl;
            // cout << "celula 7:" << cells[7] << endl;

            
        }



        // desenha eixos inclinados em viewer1
        p1.x = 0; p1.y = 0; p1.z = 0;
        p2.x = 1000.f*cos_xy; p2.y = 1000.f*sin_xy; p2.z = 0;
        viewer->addArrow<pcl::PointXYZ>(p2, p1, 1.0, 0.0, 0.0, false, "x axis inclination");
        p2.x = -1000.f*sin_xy; p2.y = 1000.f*cos_xy*cos_yz; p2.z = 1000.f*sin_yz;
        viewer->addArrow<pcl::PointXYZ>(p2, p1, 0.0, 1.0, 0.0, false, "y axis inclination");
        p2.x = 0; p2.y = -1000.f*sin_yz; p2.z = 1000.f*cos_yz;
        viewer->addArrow<pcl::PointXYZ>(p2, p1, 0.0, 0.0, 1.0, false, "z axis inclination");



        // atualiza viewers
        viewer->updatePointCloud(cloud, "Kinect Cloud");
        viewer->spinOnce();
        viewer2->updatePointCloud(cloud_filtered, "Kinect Cloud");
        viewer2->spinOnce();
        viewer3->updatePointCloud(cloud_projected, "Kinect Cloud");
        viewer3->spinOnce();

        viewer4->updatePointCloud(cloud_projected_xy, "Kinect Cloud");
        viewer4->spinOnce();

        // remove shapes
        viewer->removeShape("x axis inclination");
        viewer->removeShape("y axis inclination");
        viewer->removeShape("z axis inclination");
        viewer->removeShape("plane");
        viewer4->removeText3D("celula 0");
        viewer4->removeText3D("celula 1");
        viewer4->removeText3D("celula 2");
        viewer4->removeText3D("celula 3");
        viewer4->removeText3D("celula 4");
        viewer4->removeText3D("celula 5");
        viewer4->removeText3D("celula 6");
        viewer4->removeText3D("celula 7");
        viewer4->removeText3D("celula 8");

        // printa parametros de camera para debug
        // viewer4->getCameras(cam);
        // cout << "Cam: " << endl
        //      << " - pos: (" << cam[0].pos[0] << ", "    << cam[0].pos[1] << ", "    << cam[0].pos[2] << ")" << endl
        //      << " - view: ("    << cam[0].view[0] << ", "   << cam[0].view[1] << ", "   << cam[0].view[2] << ")"    << endl
        //      << " - focal: ("   << cam[0].focal[0] << ", "  << cam[0].focal[1] << ", "  << cam[0].focal[2] << ")"   << endl;
    }
    device->stopVideo();
    device->stopDepth();
    return 0;
}